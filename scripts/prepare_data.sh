#!/usr/bin/env bash

echo 'cleaning data from input and output folders'

. scripts/env.sh

echo "data input dir: $INPUT_DIR"
echo "data input dir: $OUTPUT_DIR"

hdfs dfs -rm -r -f "$INPUT_DIR"
hdfs dfs -rm -r -f "$OUTPUT_DIR"

echo 'putting data into input folder'
hdfs dfs -put data "$INPUT_DIR"
