#!/usr/bin/env bash

. scripts/env.sh

hadoop jar target/wordcount-final.jar $INPUT_DIR $OUTPUT_DIR
