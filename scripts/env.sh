#!/usr/bin/env bash

GIT_BRANCH=$(git branch | grep \* | cut -d ' ' -f2)
INPUT_DIR="$GIT_BRANCH-input"
OUTPUT_DIR="$GIT_BRANCH-output"