## budowanie
```bash

mvn clean compile assembly:single

```

## przygotowanie danych

```bash
hdfs dfs -put data input

```


## uruchomienie
```bash
hadoop jar target/wordcount-final.jar input output

```

## skrypty pomocnicze

Wszystkie skrypty muszą być uruchamiane z głównego katalogu projektu.

### zbudownie projektu
```bash
scripts/build.sh
```
### przygotowanie danych
```bash
scripts/prepare_data.sh
```
### uruchomienie
```bash
scripts/run.sh
```
### wszystko na raz
```bash
scripts/all.sh
```