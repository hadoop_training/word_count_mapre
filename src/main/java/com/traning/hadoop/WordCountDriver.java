package com.traning.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

public class WordCountDriver extends Configured implements Tool {

    private static Logger logger = Logger.getLogger(WordCountDriver.class);

    public static void main(String[] args) {
        try {
            int res = ToolRunner.run(new Configuration(), new WordCountDriver(), args);
            System.exit(res);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public int run(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Your job name");

        job.setJarByClass(WordCountDriver.class);

        if (args.length < 2) {
            logger.warn("to run this jar are necessary at 2 parameters \"" + job.getJar() + " input_files output_directory");
            return 1;
        }

        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        Path input = new Path(args[0]);
        Path output = new Path(args[1]);
        TextInputFormat.setInputPaths(job, input);
        TextOutputFormat.setOutputPath(job, output);

        job.waitForCompletion(true);

        return 0;
    }
}
